# ionic cached image

Ionic build for desktop using electron

## How To

#### 1. Create new ionic project
```
ionic start ionicElectron blank --type=angular
```

#### 2. Go to project directory
```
cd ionicElectron
```

#### 3. Install dependencies
```
npm i
```

#### 4. Install and add electron as dev dependency
```
npm install electron --save-dev
```

#### 5. Install and add electron-packager as dev dependency
```
npm install electron-packager --save-dev
```

#### 6. Add electron-quick-start to folder electron as electron base build
```
git clone https://github.com/electron/electron-quick-start electron
```

#### 7. Edit src/index.html
From:
```
  <base href="/" />
```
To:
```
  <base href=“./“ /
```

#### 8. Edit package.json
Add:
```
  "main": "electron/main.js",
```

#### 9. Edit electron/main.js
From:
```
mainWindow.loadFile('index.html')
```
To:
```
mainWindow.loadFile('www/index.html')
```

#### 10. Build Ionic for web version
```
Ionic build
```

#### 11. Build web version to desktop using electron-packager
```
electron-packager --overwrite . IonicElectron
```
